from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib import auth
from accounts.forms import RegistrationForm, Driver_Form, LoginForm, RequestForm
from accounts.forms import SearchForm, SearchJoinForm, EditProfileForm, EditVehicleForm
#we want to use data stored in the User model
#from django.contrib.auth.models import User
#deny some users(not logged in, etc)
#a decorator allows you to add some functions to a view
from django.contrib.auth.decorators import login_required
from accounts.models import UserInfo, Ride
from django.contrib.auth.models import User
from django.core.mail import send_mail

# Create your views here.
def home(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            return HttpResponseRedirect('/accounts/driver/home/')
        else:
            return HttpResponseRedirect('/accounts/passenger/home/')
    else:
        return render(request, 'home.html')


def register(request, isdriver=False):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            password = form.cleaned_data['password2']
            vehicle_type = request.POST.get('vehicle_type', None)
            vehicle_max_passenger = request.POST.get('vehicle_max_passenger', None)
            isDriver = request.POST.get('isDriver', None)
            isDriver = False if isDriver == None else True
            if isDriver == True and (vehicle_type == 'none' or vehicle_max_passenger == 'none') :

                return render(request, 'accounts/reg_form.html', {'form': form, 'message': 'Please fill in your vehicle infomation.'})

            user = User.objects.create_user(username=username,
                    password=password, email=email, first_name=first_name,
                    last_name=last_name)
            user_profile = UserInfo(user=user, vehicle_type=vehicle_type, vehicle_max_passenger=vehicle_max_passenger, isDriver=isDriver)
            user_profile.save()
            auth.logout(request)
            return HttpResponseRedirect('/accounts/')
        else:
            return render(request, 'accounts/reg_form.html', {'form': form})
    else:
        auth.logout(request)
        form = RegistrationForm()
        args = {'form': form}
        return render(request, 'accounts/reg_form.html', args)

def passenger_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = auth.authenticate(username=username, password=password)

            if user is not None:
                auth.login(request, user)
                user_profile = UserInfo.objects.filter(user=request.user)[0]
                user_profile.login_status = 'PASSENGER'
                user_profile.save()
                return HttpResponseRedirect('/accounts/passenger/')

            else:
                  return render(request, 'accounts/passenger_login.html', {'form': form,
                               'message': 'Wrong password. Please try again.'})
    else:
        if request.user.is_authenticated:
            user_profile = UserInfo.objects.filter(user=request.user)[0]
            if user_profile.login_status == 'PASSENGER':
                return HttpResponseRedirect('/accounts')
            else:
                return HttpResponseRedirect('/accounts/driver/')
        else:
            form = LoginForm()

    return render(request, 'accounts/passenger_login.html', {'form': form})
def passenger_display(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            history_request_list = Ride.objects.filter(user=request.user, isFinished=True)
            current_request_list = Ride.objects.filter(user=request.user, isFinished=False)
            if current_request_list.count() == 0:
                current_request = None
                driver = None
            else:
                current_request = current_request_list[0]
                if current_request.isPicked == True:
                    driver = User.objects.filter(id=current_request.driver_id)[0]
                else:
                    driver = None
            return render(request, 'accounts/passenger.html',
                    {'history_request_list': history_request_list, 'current_request': current_request,
                    'driver': driver, 'message': request.session.get('message', default=None)})
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def make_request(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            if request.method == 'POST':
                form = RequestForm(request.POST)
                if form.is_valid():
                    if form.cleaned_data['share_ride'] == None:
                        isSharable = False
                    else:
                        isSharable = True
                    newride = Ride(destination=form.cleaned_data['destination'],
                                number_passenger=form.cleaned_data['number_passenger'],
                                passenger_id=request.user.id,
                                vehicle_type=form.cleaned_data['vehicle_type'],
                                arrival_hour=form.cleaned_data['arrival_hour'],
                                arrival_minute=form.cleaned_data['arrival_minute'],
                                isSharable=isSharable,
                                )
                    newride.save()
                    newride.user.add(request.user)

                    request.session['message'] = 'You have required a ride.'
                    return HttpResponseRedirect('/accounts/passenger')
            else:
                requested_rides = Ride.objects.filter(user=request.user, isPicked=False)
                if len(requested_rides) != 0:
                    request.session['message'] = 'You have unfinished ride.'
                    return HttpResponseRedirect('/accounts/passenger')
                else:
                    form = RequestForm()
                    args = {'form': form}
                    return render(request, 'accounts/make_request.html', args)
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def edit_request(request, ride_id):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            if request.method == 'POST':
                form = RequestForm(request.POST)
                if form.is_valid():
                    user_request = Ride.objects.filter(id=ride_id)[0]
                    user_request.destination = form.cleaned_data['destination']
                    user_request.arrival_time = form.cleaned_data['arrival_time']
                    user_request.number_passenger = form.cleaned_data['number_passenger']
                    user_request.vehicle_type = form.cleaned_data['vehicle_type']
                    user_request.save()
                    return redirect('../../../')

            else:
                user_request = Ride.objects.filter(id=ride_id)[0]
                if user_request.isPicked == True:
                    request.session['message'] = 'This ride has been picked.'
                    return HttpResponseRedirect('/accounts/passenger/')
                form = RequestForm(instance=user_request)
                return render(request, 'accounts/edit_request.html', {'form':form})
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def delete_request(request, ride_id):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            ride = Ride.objects.filter(id=ride_id)[0]
            if ride.isPicked == True:
                request.session['message'] = 'This ride has been picked.'
                return HttpResponseRedirect('/accounts/passenger/')
            ride.delete()
            return HttpResponseRedirect('/accounts/passenger/')
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def join(request, ride_id):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            join_ride = Ride.objects.filter(id=ride_id)[0]
            if join_ride.isPicked == False and join_ride.isFinished == False and join_ride.isSharable == True:
                join_ride.number_passenger += 1;
                join_ride.user.add(request.user)
                join_ride.save()
                return HttpResponseRedirect('/accounts/passenger')
            else:
                request.session['message'] = 'Join failed'
                return HttpResponseRedirect('/accounts/passenger')
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def driver_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                userinfo = UserInfo.objects.filter(user=request.user)[0]
                if(userinfo.isDriver == True):
                    user_profile = UserInfo.objects.filter(user=request.user)[0]
                    user_profile.login_status = 'DRIVER'
                    user_profile.save()
                    ride_list = Ride.objects.all()
                    return HttpResponseRedirect('/accounts/driver')
                else:
                    auth.logout(request)
                    return render(request, 'accounts/driver_login.html',
                    {'form': form, 'message': 'You are not a DRIVER.Please login as a passenger.'})
            else:
            # 登陆失败
              return render(request, 'accounts/driver_login.html', {'form': form,
                           'message': 'Wrong password. Please try again.'})

    else:
        if request.user.is_authenticated :
            user_profile = UserInfo.objects.filter(user=request.user)[0]
            if user_profile.login_status == 'DRIVER':
                return HttpResponseRedirect('/accounts/driver/')
            else:
                return HttpResponseRedirect('/accounts/passenger/')
        else:
            form = LoginForm()
            return render(request, 'accounts/driver_login.html', {'form': form})

def driver_display(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            history_pickup_list = Ride.objects.filter(driver_id=request.user.id, isFinished=True)
            current_pickup_list = Ride.objects.filter(driver_id=request.user.id, isFinished=False)
            if current_pickup_list.count() == 0:
                current_pickup = None
            else:
                current_pickup = current_pickup_list[0]
            return render(request, 'accounts/driver.html',
                    {'history_pickup_list': history_pickup_list, 'current_pickup': current_pickup,
                    'message': request.session.get('message', default=None)})
        else:
            return HttpResponseRedirect('/accounts/passenger')
    else:
        return HttpResponseRedirect('/accounts/')

def pickup(request, ride_id):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            selected_ride = Ride.objects.filter(id=ride_id)[0]
            driver_info = UserInfo.objects.filter(user=request.user)[0]
            if selected_ride.number_passenger <= driver_info.vehicle_max_passenger and selected_ride.vehicle_type == driver_info.vehicle_type:
                selected_ride.isPicked = True
                selected_ride.driver_id = request.user.id
                selected_ride.user.add(request.user)
                selected_ride.save()
                Rider_group = User.objects.filter(ride = selected_ride)
                for rider in Rider_group:
                    send_mail(
                    'Hi, there! Your driver is picking you up soon!',
                    'Dear,'+rider.first_name+ " Your ride to " + selected_ride.destination + " is picking up by driver "+request.user.first_name,
                    'god.niko@gmail.com',
                    [rider.email],
                    fail_silently=False,
                    )
                return HttpResponseRedirect('/accounts/driver')
            else:
                return render(request, 'accounts/driver_search.html', {'message': 'You can\'t pick up this ride.'})
        else:
            return HttpResponseRedirect('/accounts/passenger/')
    else:
        return HttpResponseRedirect('/accounts/')

def finish(request, ride_id):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            selected_ride = Ride.objects.filter(id=ride_id)[0]
            if selected_ride.isFinished == False:
                selected_ride.isFinished = True
                selected_ride.save()
                request.session['message'] = ''
                return HttpResponseRedirect('/accounts/driver')
            else:
                return HttpResponseRedirect('/accounts/driver')
        else:
            return HttpResponseRedirect('/accounts/passenger/')
    else:
        return HttpResponseRedirect('/accounts/')


@login_required
def driver_view_profile(request):
    user_info = UserInfo.objects.filter(user=request.user)[0]
    args = {'user': request.user, 'user_info':user_info}
    return render(request, 'accounts/driver_profile.html', args)

@login_required
def passenger_view_profile(request):
    # user_info = UserInfo.objects.filter(user=request.user)[0]
    # args = {'user': request.user, 'user_info':user_info}
    return render(request, 'accounts/passenger_profile.html')

@login_required
def driver_edit_basic_profile(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            if request.method == 'POST':
                form = EditProfileForm(request.POST)
                if form.is_valid():
                    user_basic = User.objects.filter(id=request.user.id)[0]
                    user_basic.email = form.cleaned_data['email']
                    user_basic.first_name = form.cleaned_data['first_name']
                    user_basic.last_name = form.cleaned_data['last_name']
                    user_basic.save()
                    return redirect('/accounts/driver/profile/')
            else:
                #user_info = UserInfo.objects.filter(user=request.user)[0]
                form = EditProfileForm(instance=request.user)
                return render(request, 'accounts/driver_edit_profile.html', {'form':form})
        else:
            return HttpResponseRedirect('/accounts/passenger/')
    else:
        return HttpResponseRedirect('/accounts/')

@login_required
def passenger_edit_basic_profile(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            if request.method == 'POST':
                form = EditProfileForm(request.POST)
                if form.is_valid():
                    user_basic = User.objects.filter(id=request.user.id)[0]
                    user_basic.email = form.cleaned_data['email']
                    user_basic.first_name = form.cleaned_data['first_name']
                    user_basic.last_name = form.cleaned_data['last_name']
                    user_basic.save()
                    return redirect('/accounts/passenger/profile/')
            else:
                #user_info = UserInfo.objects.filter(user=request.user)[0]
                form = EditProfileForm(instance=request.user)
                return render(request, 'accounts/passenger_edit_profile.html', {'form':form})
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')


@login_required
def driver_edit_vehicle_profile(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            if request.method == 'POST':
                form = EditVehicleForm(request.POST)
                if form.is_valid():
                    user_info = UserInfo.objects.filter(user=request.user)[0]
                    user_info.vehicle_type = form.cleaned_data['vehicle_type']
                    user_info.vehicle_max_passenger = form.cleaned_data['vehicle_max_passenger']
                    user_info.save()
                    return redirect('/accounts/driver/profile/')
            else:
                user_info = UserInfo.objects.filter(user=request.user)[0]
                form = EditVehicleForm(instance=user_info)
                return render(request, 'accounts/driver_edit_profile.html', {'form':form})
        else:
            return HttpResponseRedirect('/accounts/passenger/')
    else:
        return HttpResponseRedirect('/accounts/')


def driver_search(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'DRIVER':
            if request.method == 'POST':
                form = SearchForm(request.POST)
                if form.is_valid():
                    driver_profile = UserInfo.objects.filter(user=request.user)[0]
                    destination=form.cleaned_data['destination']
                    earliest_hour = form.cleaned_data['earliest_hour']
                    earliest_minute = form.cleaned_data['earliest_minute']
                    latest_hour = form.cleaned_data['latest_hour']
                    latest_minute = form.cleaned_data['latest_minute']
                    number_passenger = form.cleaned_data['number_of_passenger']
                    max_passenger=UserInfo.objects.filter(user=request.user)[0].vehicle_max_passenger
                    rides = Ride.objects.filter(isPicked=False, isFinished=False, vehicle_type=driver_profile.vehicle_type)
                    if int(number_passenger) > max_passenger:
                        return render(request, 'accounts/driver_search_form.html', {'form': form, 'message': 'The number of passenger is too large.'})
                    pickable_rides = []
                    for ride in rides:
                        if destination is not None:
                            if ride.destination != destination:
                                continue
                        elif earliest_hour != 99:
                            if earliest_hour > ride.arrival_hour or (earliest_hour <= ride.arrival_hour and earliest_minute > ride.arrival_minute):
                                continue
                        elif latest_hour != 99:
                            if latest_hour < ride.arrival_hour or (latest_hour >= ride.arrival_hour and latest_hour_minute < ride.arrival_minute):
                                continue
                        elif number_passenger != 99:
                            if int(number_passenger) < ride.number_passenger:
                                continue
                        pickable_rides.append(ride)
                    return render(request, 'accounts/search_pickup.html', {'ride_not_picked_list': pickable_rides})
                else:
                    return render(request, 'accounts/driver.html', {'form': form})
            else:
                picked_rides = Ride.objects.filter(driver_id=request.user.id, isFinished=False)
                requested_rides = Ride.objects.filter(user=request.user, isFinished=False)
                if len(picked_rides) != 0 or len(requested_rides) != 0:
                    request.session['message'] = 'You have unfinished ride.'
                    return HttpResponseRedirect('/accounts/driver')
                form = SearchForm()
                args = {'form': form}
                return render(request, 'accounts/driver_search_form.html', args)
        else:
            return HttpResponseRedirect('/accounts/passenger/')
    else:
        return HttpResponseRedirect('/accounts/')

def passenger_search(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        if user_profile.login_status == 'PASSENGER':
            if request.method == 'POST':
                form = SearchJoinForm(request.POST)
                if form.is_valid():
                    destination=form.cleaned_data['destination']
                    earliest_hour = form.cleaned_data['earliest_hour']
                    earliest_minute = form.cleaned_data['earliest_minute']
                    latest_hour = form.cleaned_data['latest_hour']
                    latest_minute = form.cleaned_data['latest_minute']
                    vehicle_type = form.cleaned_data['vehicle_type']
                    rides = Ride.objects.filter(isPicked=False, isFinished=False, isSharable=True)
                    number_passenger = form.cleaned_data['number_of_passenger']
                    joinable_rides = []
                    for ride in rides:
                        if destination is not None:
                            if ride.destination != destination:
                                continue
                        elif earliest_hour != 99:
                            if earliest_hour > ride.arrival_hour or (earliest_hour <= ride.arrival_hour and earliest_minute > ride.arrival_minute):
                                continue
                        elif latest_hour != 99:
                            if latest_hour < ride.arrival_hour or (latest_hour >= ride.arrival_hour and latest_hour_minute < ride.arrival_minute):
                                continue
                        elif vehicle_type != 99:
                            if ride.vehicle_type != vehicle_type:
                                continue
                        elif number_passenger != 99:
                            if ride.number_passenger != number_passenger:
                                continue
                        joinable_rides.append(ride)
                    return render(request, 'accounts/search_join.html', {'ride_not_picked_list': joinable_rides})
                else:
                    return render(request, 'accounts/passenger.html', {'form': form})
            else:
                picked_rides = Ride.objects.filter(driver_id=request.user.id, isFinished=False)
                requested_rides = Ride.objects.filter(user=request.user, isFinished=False)
                if len(picked_rides) != 0 or len(requested_rides) != 0:
                    request.session['message'] = 'You have unfinished ride.'
                    return HttpResponseRedirect('/accounts/driver')
                form = SearchJoinForm()
                args = {'form': form}
                return render(request, 'accounts/passenger_search_form.html', args)
        else:
            return HttpResponseRedirect('/accounts/driver/')
    else:
        return HttpResponseRedirect('/accounts/')

def logout(request):
    if request.user.is_authenticated:
        user_profile = UserInfo.objects.filter(user=request.user)[0]
        user_profile.login_status = ''
        user_profile.save()
        auth.logout(request)
    return render(request, 'accounts/logout.html')
