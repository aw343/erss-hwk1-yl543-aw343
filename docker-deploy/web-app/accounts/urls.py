from django.urls import path
from . import views
from django.contrib.auth.views import LoginView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.home),
    #path('driver_reg/', views.driver_register),
    path('register/', views.register, name='register'),
    path('passenger_login/', views.passenger_login),
    path('passenger/', views.passenger_display),
    path('passenger/profile/', views.passenger_view_profile),
    path('passenger/profile/edit_basic/', views.passenger_edit_basic_profile),
    path('passenger/home/', auth_views.LoginView.as_view(template_name='passenger_home.html')),
    path('passenger/request/', views.make_request),
    path('passenger/request/edit/<int:ride_id>/', views.edit_request),
    path('passenger/request/delete/<int:ride_id>/', views.delete_request),
    path('passenger/search/', views.passenger_search),
    path('passenger/join/<int:ride_id>/', views.join),
    path('driver/home/', auth_views.LoginView.as_view(template_name='driver_home.html')),
    path('driver_login/', views.driver_login),
    path('driver/search/', views.driver_search),
    path('driver/pickup/<int:ride_id>/', views.pickup),
    path('driver/finish/<int:ride_id>/', views.finish),
    path('driver/', views.driver_display),
    path('driver/profile/', views.driver_view_profile),
    path('driver/profile/edit_basic/', views.driver_edit_basic_profile),
    path('driver/profile/edit_vehicle/', views.driver_edit_vehicle_profile),
    path('logout/',views.logout),
#    path('profile/', views.view_profile, name='profile'),
    # path('profile/edit/', views.edit_profile, name='edit_profile'),
]
